/*
Navicat MySQL Data Transfer

Source Server         : 192.168.23.128
Source Server Version : 80016
Source Host           : 192.168.23.128:33306
Source Database       : ry_meteor

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2019-12-05 13:49:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_meteor_cmdlog
-- ----------------------------
DROP TABLE IF EXISTS `t_meteor_cmdlog`;
CREATE TABLE `t_meteor_cmdlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(40)  NOT NULL COMMENT '服务器地址',
  `appname` varchar(80)  NOT NULL COMMENT '应用名称',
  `cmd` varchar(40)  NOT NULL COMMENT '命令',
  `create_person_id` varchar(20)  NOT NULL COMMENT '操作人员ID',
  `create_person_name` varchar(20)  NOT NULL COMMENT '操作人员名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  ;

-- ----------------------------
-- Records of t_meteor_cmdlog
-- ----------------------------

-- ----------------------------
-- Table structure for t_meteor_machineinfo
-- ----------------------------
DROP TABLE IF EXISTS `t_meteor_machineinfo`;
CREATE TABLE `t_meteor_machineinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(40)  NOT NULL COMMENT '机器名称',
  `login_type` varchar(4)   DEFAULT NULL COMMENT '登录类型',
  `rsa_id` int(11) DEFAULT NULL COMMENT 'RSA',
  `username` varchar(40)     DEFAULT NULL COMMENT '登陆账号',
  `password` varchar(200)     DEFAULT NULL COMMENT '密码',
  `port` int(11) DEFAULT '22' COMMENT 'SSH端口号',
  `arthas_ip` varchar(40)   DEFAULT NULL COMMENT 'agentIP',
  `arthas_port` int(11) NOT NULL DEFAULT '8563' COMMENT 'agent端口',
  `arthas_agent_id` varchar(20)   DEFAULT NULL COMMENT 'agentId',
  `server_status` varchar(4)     NOT NULL COMMENT '服务器连接状态',
  `module_name` varchar(40)   DEFAULT NULL COMMENT '模块名称',
  `status` varchar(8)     NOT NULL DEFAULT 'valid' COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_person_id` varchar(20)     NOT NULL COMMENT '创建人员ID',
  `create_person_name` varchar(80)     NOT NULL COMMENT '操作人员名称',
  PRIMARY KEY (`id`,`hostname`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_meteor_machineinfo
-- ----------------------------
INSERT INTO `t_meteor_machineinfo` VALUES ('1', '192.168.23.128', 'nrsa', null, 'root', 'reywong', '22', '192.168.23.128', '8563', '001', '1', 'ICS', 'valid', '2019-12-03 02:23:29', '52', '系统管理员');
INSERT INTO `t_meteor_machineinfo` VALUES ('2', '192.168.23.128', 'rsa', '1', '', '', '22', '192.168.23.1', '7777', '159912043644928', '2', 'DEMO', 'valid', '2019-12-13 03:02:59', '52', '系统管理员');


-- ----------------------------
-- Table structure for t_meteor_machineinfo_privilege
-- ----------------------------
DROP TABLE IF EXISTS `t_meteor_machineinfo_privilege`;
CREATE TABLE `t_meteor_machineinfo_privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machine_id` int(11) NOT NULL COMMENT '服务器ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8  ;

-- ----------------------------
-- Records of t_meteor_machineinfo_privilege
-- ----------------------------
INSERT INTO `t_meteor_machineinfo_privilege` VALUES ('25', '1', '52', '2019-12-04 06:15:28');
INSERT INTO `t_meteor_machineinfo_privilege` VALUES ('26', '1', '54', '2019-12-04 06:15:28');
INSERT INTO `t_meteor_machineinfo_privilege` VALUES ('27', '1', '58', '2019-12-04 06:15:28');
INSERT INTO `t_meteor_machineinfo_privilege` VALUES ('28', '1', '77', '2019-12-04 06:15:28');
INSERT INTO `t_meteor_machineinfo_privilege` VALUES ('29', '1', '78', '2019-12-04 06:15:28');
INSERT INTO `t_meteor_machineinfo_privilege` VALUES ('30', '1', '80', '2019-12-04 06:15:28');

-- ----------------------------
-- Table structure for t_sec_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_sec_menu`;
CREATE TABLE `t_sec_menu` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(2000) DEFAULT NULL,
  `PARENT_ID` bigint(20) DEFAULT NULL,
  `PRIVILEGE_CODE` varchar(200) DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `order_sn` int(11) DEFAULT NULL,
  `ICON` varchar(100) DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  `code` varchar(16) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `unique_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sec_menu
-- ----------------------------
INSERT INTO `t_sec_menu` VALUES ('153', '权限管理', '权限管理', null, '', null, '2', 'users', 'valid', '02');
INSERT INTO `t_sec_menu` VALUES ('155', '系统管理', '系统管理', null, '', null, '1', 'desktop', 'valid', '01');
INSERT INTO `t_sec_menu` VALUES ('156', '字典管理', '字典管理', '155', 'sys_dict', '/sys/dict/index', null, null, 'valid', '0106');
INSERT INTO `t_sec_menu` VALUES ('158', '管理员管理', '管理员管理', '153', 'sec_manager', '/sec/manager/index', null, null, 'valid', '0202');
INSERT INTO `t_sec_menu` VALUES ('159', '用户管理', '用户管理', '153', 'sec_user', '/sec/user/index', null, null, 'valid', '0203');
INSERT INTO `t_sec_menu` VALUES ('161', '菜单管理', '菜单管理', '153', 'sec_menu', '/sec/menu/index', null, null, 'valid', '0204');
INSERT INTO `t_sec_menu` VALUES ('162', '角色管理', '菜单管理', '153', 'sec_role', '/sec/role/index', null, null, 'valid', '0201');
INSERT INTO `t_sec_menu` VALUES ('163', '文件管理', '文件管理', '155', 'sys_xfile', '/sys/xfile/index', null, null, 'valid', '0103');
INSERT INTO `t_sec_menu` VALUES ('165', '数据字典组管理', '数据字典组管理', '155', 'sys_dictgroup', '/sys/dictgroup/index', null, null, 'valid', '0105');
INSERT INTO `t_sec_menu` VALUES ('170', '代码生成管理', '代码生成管理', '155', 'sys_code', '/sys/code/index', null, null, 'valid', '0102');
INSERT INTO `t_sec_menu` VALUES ('171', 'Meteor', 'Meteor', null, '', null, null, 'desktop', 'valid', '03');
INSERT INTO `t_sec_menu` VALUES ('176', 'Dashboard', 'Dashboard', '171', 'meteor_appinfo', '/meteor/appinfo/index', null, null, 'valid', '0301');
INSERT INTO `t_sec_menu` VALUES ('177', '工作台', '工作台', '171', 'meteor_workplatform', '/meteor/workplatform/index', null, null, 'valid', '0303');
INSERT INTO `t_sec_menu` VALUES ('178', '控制台管理', '控制台管理', null, '', null, null, 'list-alt', 'valid', '08');
INSERT INTO `t_sec_menu` VALUES ('179', 'arthas控制台', 'arthas控制台', '178', 'meteor_console', '/meteor/console/index', null, null, 'valid', '0801');
INSERT INTO `t_sec_menu` VALUES ('180', 'linux系统控制台', 'linux系统控制台', '178', 'meteor_websocket', '/meteor/websocket/index', null, null, 'valid', '0802');
INSERT INTO `t_sec_menu` VALUES ('183', '服务器列表', '服务器列表', '171', 'meteor_param', '/meteor/param/index', null, null, 'valid', '0300');
INSERT INTO `t_sec_menu` VALUES ('185', 'arthas执行器', 'arthas执行器', '171', 'meteor_arthas', '/meteor/arthas/index', null, null, 'invalid', '0304');
INSERT INTO `t_sec_menu` VALUES ('217', 'ssh执行器', 'ssh执行器', '171', 'meteor_ssh', '/meteor/ssh/index', null, null, 'invalid', '0305');
INSERT INTO `t_sec_menu` VALUES ('218', '线程管理', '线程管理', '171', 'meteor_thread', '/meteor/thread/index', null, null, 'valid', '0302');
INSERT INTO `t_sec_menu` VALUES ('222', '监控管理', '监控管理', null, '', null, null, 'credit-card', 'valid', '04');
INSERT INTO `t_sec_menu` VALUES ('223', '方法监控', '方法监控', '222', 'meteor_monitor', '/meteor/monitor/index', null, null, 'valid', '0401');
INSERT INTO `t_sec_menu` VALUES ('224', '调用链路', '调用链路', '222', 'meteor_trace', '/meteor/trace/index', null, null, 'valid', '0402');
INSERT INTO `t_sec_menu` VALUES ('225', '被调用链路', '被调用链路', '222', 'meteor_stack', '/meteor/stack/index', null, null, 'valid', '0403');
INSERT INTO `t_sec_menu` VALUES ('226', '时光隧道', '时光隧道', '222', 'meteor_timetunnel', '/meteor/timetunnel/index', null, null, 'valid', '0405');
INSERT INTO `t_sec_menu` VALUES ('227', '调用数据监控', '调用数据监控', '222', 'meteor_watch', '/meteor/watch/index', null, null, 'valid', '0404');
INSERT INTO `t_sec_menu` VALUES ('228', '查看调用信息', '查看调用信息', '222', 'meteor_watch', '/meteor/watch/index', null, null, 'valid', '0406');
INSERT INTO `t_sec_menu` VALUES ('230', '代码查看', '代码查看', '222', 'meteor_javacode', '/meteor/javacode/index', null, null, 'valid', '0407');
INSERT INTO `t_sec_menu` VALUES ('231', 'Meteor权限管理', 'Meteor权限管理', null, '', null, null, 'user', 'valid', '07');
INSERT INTO `t_sec_menu` VALUES ('232', '服务器管理', '服务器管理', '231', 'meteor_machineinfo', '/meteor/machineinfo/index', null, null, 'valid', '0701');
INSERT INTO `t_sec_menu` VALUES ('233', '常用工具', '常用工具', null, '', null, null, 'mortar-board', 'valid', '05');

-- ----------------------------
-- Table structure for t_sec_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sec_role`;
CREATE TABLE `t_sec_role` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(200) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(200) DEFAULT NULL COMMENT '编码',
  `DESCRIPTION` varchar(2000) DEFAULT NULL COMMENT '描述',
  `status` varchar(20) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_code` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sec_role
-- ----------------------------
INSERT INTO `t_sec_role` VALUES ('4', '管理员', 'admin', '管理员', 'valid');
INSERT INTO `t_sec_role` VALUES ('6', '开发', 'service', '开发', 'valid');

-- ----------------------------
-- Table structure for t_sec_role_privilege
-- ----------------------------
DROP TABLE IF EXISTS `t_sec_role_privilege`;
CREATE TABLE `t_sec_role_privilege` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `PRIVILEGE_TITLE` varchar(200) DEFAULT NULL,
  `PRIVILEGE_CODE` varchar(100) DEFAULT NULL,
  `FUNCTION_LIST` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sec_role_privilege
-- ----------------------------
INSERT INTO `t_sec_role_privilege` VALUES ('14', '4', '代码生成管理', 'sys_code', 'generate');
INSERT INTO `t_sec_role_privilege` VALUES ('15', '4', '菜单管理', 'sec_menu', 'edit|delete|create|list');
INSERT INTO `t_sec_role_privilege` VALUES ('20', '4', '字典管理', 'sys_dict', 'edit|delete|create|list');
INSERT INTO `t_sec_role_privilege` VALUES ('26', '4', '管理员管理', 'sec_manager', 'delete|create|list|edit');
INSERT INTO `t_sec_role_privilege` VALUES ('27', '4', '角色管理', 'sec_role', 'edit|delete|create|list');
INSERT INTO `t_sec_role_privilege` VALUES ('28', '4', '用户管理', 'sec_user', 'edit|delete|create|list');
INSERT INTO `t_sec_role_privilege` VALUES ('29', '5', '菜单管理', 'sec_menu', 'edit|delete|create|list');
INSERT INTO `t_sec_role_privilege` VALUES ('30', '6', '角色管理', 'sec_role', 'edit|delete|create|list');
INSERT INTO `t_sec_role_privilege` VALUES ('31', '6', '文件管理', 'sys_xfile', 'edit|delete|create|list');
INSERT INTO `t_sec_role_privilege` VALUES ('32', '6', '用户管理', 'sec_user', 'edit|deletelist');

-- ----------------------------
-- Table structure for t_sec_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sec_user`;
CREATE TABLE `t_sec_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `user_type` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `realname` varchar(4) DEFAULT NULL COMMENT '是否实名0-未实名 1-已实名',
  `certificate_type` varchar(20) DEFAULT NULL COMMENT '证件类型',
  `certificate_no` varchar(80) DEFAULT NULL COMMENT '证件号码',
  `address` varchar(45) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `birthday` datetime DEFAULT NULL,
  `province` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_login_name` (`login_name`) USING BTREE,
  UNIQUE KEY `unique_email` (`email`),
  UNIQUE KEY `unique_mobile` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sec_user
-- ----------------------------
INSERT INTO `t_sec_user` VALUES ('52', 'admin', 'd48308051ac59fe4157958d063c62d22', '系统管理员', 'admin', 'status', '12345678911', null, null, null, '', '2018-04-19 13:30:57', 'valid', '2018-04-19 00:00:00', null);


-- ----------------------------
-- Table structure for t_sec_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sec_user_role`;
CREATE TABLE `t_sec_user_role` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sec_user_role
-- ----------------------------
INSERT INTO `t_sec_user_role` VALUES ('1', '58', '6');

-- ----------------------------
-- Table structure for t_sso_project
-- ----------------------------
DROP TABLE IF EXISTS `t_sso_project`;
CREATE TABLE `t_sso_project` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) DEFAULT NULL,
  `CODE` varchar(200) NOT NULL,
  `CREATE_USER_ID` bigint(20) DEFAULT NULL,
  `CREATE_USER_NAME` varchar(200) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `DOMAIN` varchar(100) DEFAULT NULL,
  `LOGIN_URL` varchar(1000) DEFAULT NULL,
  `VALID_IP` varchar(1000) DEFAULT NULL,
  `PWD_TYPE` varchar(45) DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sso_project
-- ----------------------------
INSERT INTO `t_sso_project` VALUES ('36', 'csdn', 'csdn', '52', '系统管理员', '2018-05-09 14:24:26', '', 'http://www.csdn.net', '', 'md5', 'valid');
INSERT INTO `t_sso_project` VALUES ('37', '百度', 'baidu', '52', '系统管理员', '2018-05-09 14:29:32', null, 'https://www.baidu.com', '', '', 'valid');
INSERT INTO `t_sso_project` VALUES ('38', 'zhj', 'zhj', '52', '系统管理员', '2018-05-12 10:03:21', 'http://localhost:8885/sso_client', 'http://localhost:8885/sso_client/passport/login', '', '', 'valid');

-- ----------------------------
-- Table structure for t_sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_dict`;
CREATE TABLE `t_sys_dict` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `group_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '属组',
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `code` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编码',
  `status` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
  `detail` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `sn` int(8) unsigned DEFAULT NULL COMMENT '序号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=710 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sys_dict
-- ----------------------------
INSERT INTO `t_sys_dict` VALUES ('5', 'gender', '男', 'man', 'valid', '', '1');
INSERT INTO `t_sys_dict` VALUES ('6', 'gender', '女', 'woman', 'valid', null, '2');
INSERT INTO `t_sys_dict` VALUES ('705', 'machine', '初始状态', '0', 'valid', '', '1');
INSERT INTO `t_sys_dict` VALUES ('706', 'machine', 'Meteor-Agent上传成功', '1', 'valid', '', '2');
INSERT INTO `t_sys_dict` VALUES ('707', 'machine', 'Meteor-Agent启动成功', '2', 'valid', '', '3');
INSERT INTO `t_sys_dict` VALUES ('708', 'machine', 'Meteor-Agent上传失败', '3', 'valid', '', '3');
INSERT INTO `t_sys_dict` VALUES ('709', 'machine', 'Meteor-Agent启动失败', '4', 'valid', '', '4');

-- ----------------------------
-- Table structure for t_sys_dictgroup
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_dictgroup`;
CREATE TABLE `t_sys_dictgroup` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `group_code_m` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组编码',
  `group_name` varchar(40) DEFAULT NULL COMMENT '组名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sys_dictgroup
-- ----------------------------
INSERT INTO `t_sys_dictgroup` VALUES ('6', 'gender', '性别');
INSERT INTO `t_sys_dictgroup` VALUES ('22', 'machine', '初始化机器参数');

-- ----------------------------
-- Table structure for t_sys_xfile
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_xfile`;
CREATE TABLE `t_sys_xfile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_realname` varchar(200) NOT NULL,
  `file_path` varchar(200) NOT NULL,
  `file_title` varchar(200) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `file_size` bigint(16) DEFAULT NULL,
  `user_id` bigint(16) DEFAULT NULL,
  `extension_name` varchar(16) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `is_deleted` char(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sys_xfile
-- ----------------------------
INSERT INTO `t_sys_xfile` VALUES ('1', '20190820161405066.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.9150301109343302102.8884/files/upload/user/2019/08/20/20190820161405066.xls', 'modules.xls', '/files/upload/user/2019/08/20/20190820161405066.xls', '38400', '52', '.xls', '2019-08-20 08:14:51', null);
INSERT INTO `t_sys_xfile` VALUES ('2', '20190820163201782.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.9119816484890734190.8884/files/upload/oil/2019/08/20/20190820163201782.xls', 'modules.xls', '/files/upload/oil/2019/08/20/20190820163201782.xls', '38400', '52', '.xls', '2019-08-20 08:32:17', null);
INSERT INTO `t_sys_xfile` VALUES ('3', '20190820163402722.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.9119816484890734190.8884/files/upload/oil/2019/08/20/20190820163402722.xls', '系统分布.xls', '/files/upload/oil/2019/08/20/20190820163402722.xls', '75264', '52', '.xls', '2019-08-20 08:34:28', null);
INSERT INTO `t_sys_xfile` VALUES ('4', '20190820163902157.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.9119816484890734190.8884/files/upload/oil/2019/08/20/20190820163902157.xls', '系统分布.xls', '/files/upload/oil/2019/08/20/20190820163902157.xls', '75264', '52', '.xls', '2019-08-20 08:39:22', null);
INSERT INTO `t_sys_xfile` VALUES ('5', '20190820170501388.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.5589872266182037041.8884/files/upload/oil/2019/08/20/20190820170501388.xls', 'modules.xls', '/files/upload/oil/2019/08/20/20190820170501388.xls', '38400', '52', '.xls', '2019-08-20 09:05:14', null);
INSERT INTO `t_sys_xfile` VALUES ('6', '20190820171201890.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.4438740764138812129.8884/files/upload/oil/2019/08/20/20190820171201890.xls', '系统分布.xls', '/files/upload/oil/2019/08/20/20190820171201890.xls', '75264', '52', '.xls', '2019-08-20 09:12:19', null);
INSERT INTO `t_sys_xfile` VALUES ('7', '20190820172205354.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.4184042471184398917.8884/files/upload/oil/2019/08/20/20190820172205354.xls', '系统分布.xls', '/files/upload/oil/2019/08/20/20190820172205354.xls', '75264', '52', '.xls', '2019-08-20 09:22:53', null);
INSERT INTO `t_sys_xfile` VALUES ('8', '20190821134400710.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.2964444338350861631.8884/files/upload/user/2019/08/21/20190821134400710.xls', 'modules.xls', '/files/upload/user/2019/08/21/20190821134400710.xls', '38400', '52', '.xls', '2019-08-21 05:44:08', null);
INSERT INTO `t_sys_xfile` VALUES ('9', '20190821134502598.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.2964444338350861631.8884/files/upload/user/2019/08/21/20190821134502598.xls', 'modules.xls', '/files/upload/user/2019/08/21/20190821134502598.xls', '38400', '52', '.xls', '2019-08-21 05:45:25', null);
INSERT INTO `t_sys_xfile` VALUES ('10', '20190821135203548.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.2964444338350861631.8884/files/upload/oil/2019/08/21/20190821135203548.xls', 'modules.xls', '/files/upload/oil/2019/08/21/20190821135203548.xls', '38400', '52', '.xls', '2019-08-21 05:52:36', null);
INSERT INTO `t_sys_xfile` VALUES ('11', '20190821145103864.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.4065456949334870211.8884/files/upload/oil/2019/08/21/20190821145103864.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/21/20190821145103864.xls', '17408', '52', '.xls', '2019-08-21 06:51:38', null);
INSERT INTO `t_sys_xfile` VALUES ('12', '20190821145703794.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.4903821943588480246.8884/files/upload/oil/2019/08/21/20190821145703794.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/21/20190821145703794.xls', '17408', '52', '.xls', '2019-08-21 06:57:37', null);
INSERT INTO `t_sys_xfile` VALUES ('13', '20190821150702784.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.4903821943588480246.8884/files/upload/oil/2019/08/21/20190821150702784.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/21/20190821150702784.xls', '17408', '52', '.xls', '2019-08-21 07:07:27', null);
INSERT INTO `t_sys_xfile` VALUES ('14', '20190821151101995.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.4903821943588480246.8884/files/upload/oil/2019/08/21/20190821151101995.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/21/20190821151101995.xls', '17408', '52', '.xls', '2019-08-21 07:11:19', null);
INSERT INTO `t_sys_xfile` VALUES ('15', '20190821151302324.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.4903821943588480246.8884/files/upload/oil/2019/08/21/20190821151302324.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/21/20190821151302324.xls', '17408', '52', '.xls', '2019-08-21 07:13:24', null);
INSERT INTO `t_sys_xfile` VALUES ('16', '20190821151405654.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.4903821943588480246.8884/files/upload/oil/2019/08/21/20190821151405654.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/21/20190821151405654.xls', '17408', '52', '.xls', '2019-08-21 07:14:57', null);
INSERT INTO `t_sys_xfile` VALUES ('17', '20190821154601829.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.1194270383237027749.8884/files/upload/oil/2019/08/21/20190821154601829.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/21/20190821154601829.xls', '17408', '52', '.xls', '2019-08-21 07:46:18', null);
INSERT INTO `t_sys_xfile` VALUES ('18', '20190821154800584.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.1194270383237027749.8884/files/upload/oil/2019/08/21/20190821154800584.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/21/20190821154800584.xls', '17408', '52', '.xls', '2019-08-21 07:48:05', null);
INSERT INTO `t_sys_xfile` VALUES ('19', '20190821160603148.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.4342987577868368328.8884/files/upload/oil/2019/08/21/20190821160603148.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/21/20190821160603148.xls', '17408', '52', '.xls', '2019-08-21 08:06:32', null);
INSERT INTO `t_sys_xfile` VALUES ('20', '20190821160704796.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.3207692515986753339.8884/files/upload/oil/2019/08/21/20190821160704796.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/21/20190821160704796.xls', '17408', '52', '.xls', '2019-08-21 08:07:48', null);
INSERT INTO `t_sys_xfile` VALUES ('21', '20190823101905466.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.61355503626882211.8884/files/upload/oil/2019/08/23/20190823101905466.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/23/20190823101905466.xls', '17408', '52', '.xls', '2019-08-23 02:19:55', null);
INSERT INTO `t_sys_xfile` VALUES ('22', '20190823102101370.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.61355503626882211.8884/files/upload/oil/2019/08/23/20190823102101370.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/23/20190823102101370.xls', '17408', '52', '.xls', '2019-08-23 02:21:14', null);
INSERT INTO `t_sys_xfile` VALUES ('23', '2019082310220404.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.61355503626882211.8884/files/upload/oil/2019/08/23/2019082310220404.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/23/2019082310220404.xls', '17408', '52', '.xls', '2019-08-23 02:22:41', null);
INSERT INTO `t_sys_xfile` VALUES ('24', '20190823102400136.xls', 'C:/Users/uc/AppData/Local/Temp/tomcat-docbase.61355503626882211.8884/files/upload/oil/2019/08/23/20190823102400136.xls', '加油卡批量上传模板.xls', '/files/upload/oil/2019/08/23/20190823102400136.xls', '17408', '52', '.xls', '2019-08-23 02:24:02', null);


-- ----------------------------
-- Table structure for t_meteor_rsa
-- ----------------------------
DROP TABLE IF EXISTS `t_meteor_rsa`;
CREATE TABLE `t_meteor_rsa` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `rsa_name` varchar(80) NOT NULL COMMENT '私钥名称',
  `rsa_value` varchar(5000)  NOT NULL COMMENT '私钥内容',
  `rsa_username` varchar(20)  NOT NULL COMMENT '私钥用户',
  `rsa_password` varchar(40) DEFAULT NULL COMMENT '私钥密码',
  `status` varchar(8)  NOT NULL DEFAULT 'valid' COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_person_id` varchar(20) NOT NULL COMMENT '创建人员ID',
  `create_person_name` varchar(80) NOT NULL COMMENT '创建人员姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ;

-- ----------------------------
-- Records of t_meteor_rsa
-- ----------------------------
INSERT INTO `t_meteor_rsa` VALUES ('1', '128', '-----BEGIN RSA PRIVATE KEY-----\nProc-Type: 4,ENCRYPTED\nDEK-Info: AES-128-CBC,6139BC862E7D7DB30E034780B947B988\n\nVAIgVUdtBSSr0g8D9H01HDXcrzq+sIFfTUqUQAdfpSgHn1m5vOTUs78VWt0txVIN\n2mS1Mz0RCysA9wmjMplQu6SPr7yBKTipsj2Qx4K3lEmj3hy578rgqFnaH8LMj3UL\nyqiDWSIDVIKuGLIhdZgb2XPUdKkAMR/abzQsxtMN1BATGI/jH1JDjIhT+7ILkpom\nrUNJFNuFc4oz2tpJC71TmBnVpumXtjMbtM19SbMSWaPzkuUiPEFW4xDwTN++vfej\npmsd7eWciaMKnjJPz9zUG0BPuPzP+pEzEU+At8wfKT/U/jQpS1d8bCwD6vkxkkru\nvcolMYUKt3gw6j5eLC2ZJzhnkJFERZm01CWqPcn49Pre2/6WbGqah2ThqnOmc3t5\nOvw9P9VT+Q892eVo/km87KJnv2PxmLtcr8ngPj17+1l0Wsd/Wt8SDW5k5XPOfy6r\n5IZ5ww7dF44vUSd0tAlC/4icEtTuXtQM5C9cmqyde4JswGxPVd0uYTFNYosrLdR1\n4tU7npLdfm+mu8oW+s3j+RhOdA4/b2F34yC7jRHGatuBEpVuuFgN+rG6N1GjjDVo\njKTGB0deWt0iPqIBguOb1ZaitZ/DHtqCBzKZnUzyVw/JinaKBJZ87onSlyW5mTOn\nx787IDWj44GRbZ1QMmVSeVNBRKgIJtYNGyZs577mA2LDDzLr0Ga89zQcENVlgnTm\nBD7W6ceabrZ6+5p/DDJDtLceq3E2PsVMFbgFvIDVz08POxGe83aGHk6Qf/4CVkIU\nssYZCexUJpupJV4Hx2m3CpCWYDGZ28f0cXJwlbX/TanyU7DveAF0eSy+sZ1aWSiE\nYesfspQFjjwuysyq3eYBP3vIgmk2ZGQflY6o8cB2778I9KhZVzQsWbko2ERRQAMv\nPaZ9+pKFjrAQoIYo10SsE2qPI4KNBJgLx3FwmuZskKh4rB0y1NPINPrdsYfi+fHz\nHao6JLbLqyb2XohsvYVrmVRsw/mVUvgWnOZ10fAmaKU+tXT+fyI1Q8I8zsKA5iCL\nqvQ5BtvpUg+9JVtsA0HQXeR/Q6upJUPJU6nLDRNdIOh/Gr6WiWEOMsxIq900ugA8\neFOPxAXVrjhPL6i2UNsgtDk9T/ArebWarPgNqjL8IMNn4Sj9EeWHPHuR6W/pmbO2\nEI57rWe1TVF3pOikVMQmsCP5wLnDe295Ngo+hcEJnE8oXuQHdhXdKuW1p9u++5it\naicLpOZt2yc7FxkPAGGAH6bRW3MItm84cmtiXZmflQ0DSNwhKFq20UVtdUec7M5x\nscRBfjGAFRobEN4UrmxPAWmJ8W42pNmEmbjsI6QjE92jwNZn03T+4hwXRGjqEyqR\nAr7EwJAA9ILrxdD2uhcXogGpdp8cnI7Ttw04Plsks0+by4i7FP0Ee0+SEYwlPs+j\nnti1L8mvoanGJxoUC+8IrxkQshYUIZr87xC15WmmZHA9DFR+o9c7uRS5G8Rb/tMg\nhqf5SuGb9WEAdLoIHTBn2cRo2FlsvoJP/Okb3IkCM3nJ3Rul1jy9F1a/3s7UR5FT\n7wredD45ZYq0V9WabOgbqgvBHtrtMcdxEjE/yk7RpCtmF94IimGVBfey8rAe0ODc\n/Leu5Mj2XrZdiezJrk3veFcE5/lqeABFlDroRkBV2F3IKlWlmZ5t/4/yhlQhFc9R\nO7/tWErGBUeq0nKaJDPZ6r6f4Ug9byps4qzL0D8PpCFVF6oJOudvnvfMlkW/2eJj\nwaCxjwArbj974zcrPM/2PGhSbtrYxi1UulULPQdA9bvL0ggfou9nZVUeRtZBkY1A\nKuDxgDTNilJQ7vJfvdp09emnr2k8VBCuwtY0enNvYuer1TgxEi3+ErmRyD3jJ1d+\nyWDnO4LWVP4rF98eFD/FtI3IZfkrck07ehyzutDm+34M6qcD/0s8eBqrsNSmcOFQ\nQcnW7AUpgfJSzn0/VYig2TR6mOo/i9aeNb5VwV2nbiSKnHvPznH04lMeJhqkqsWg\nPOdDczITAtTgtl4Vm0ofdmaoKXpXUQrS6EZ2f5aZFa06MWvmusOOAA/gib9GOhwZ\nvajKLN/fu2RsJZFh3NQslgKEJ0UNvOGFGlsQm+QDNC76MPmPHtuRcfX+iLn6NWkf\nFn3ZxQaLLm5volUBXve3OvWAzPlN0VfD+iGBc8HqrYYVH1rOIJUtwykPDZdMaTGL\nLzNVmmtS7sR5+4JSTzwlipEz1i1UUDRw2RW8XixX4EVuG4Ws3Pq+Bld31sNW6Tg+\nwRfmwtWFK5TQwLyb8TOW1Vm4/07eQrmOD+2M9XUSTVt2Vod9T3/BHoB4DtzwiTpO\nKziR8TGNezjtj+J+XY/S4sooxaE7rVr/gOo4RaYsDUnLATRkJtiKeYlHR8xywZ1G\nlR0zGi4O1zA/tCXy612ba2OoyLYhx2OLa3peFaIzn933zkKyPADtq7pFVdTc5oCI\n71Fl+128BvPOUHqvnR4LT8P0E3oi5WQ2/061gUpVMqIrfjzfmsqZUyFicV5p0/Mh\ndLdTglpcimM3oN2Zm3gXXOf+MY+DNrVX2WIg9TTau01jQln2rNHdoVJk6QwL1aS3\nMWCRaFpAUa6jiqCp0bgeIfVtVWft1fPW8Lk6HgQ/1XVpOmFgetHKHx5/qefQLX1f\nEj0BYH/JFhKz5hkEnWL5WDOYINlgWqxVewT68uPneWJ8XikGxBy/DNIIlVtohqiw\nH6lIB9hDr55mMUZfNbpUwRo7oWUrSo/PWRsNQE0TpaIZmhw4Zy28d6kKNLYlkPH1\n/P3g62U+nsG4tZIclBMZBiy9LSkG0lKhjgghb42PNxtzy6uD5ErZrAEn54vww0Y3\ndJ8TkwadXTH48Erwq9WJsKcmcej8BXbybLgWIItUzXLEL5nVcbAYCJf0Fwc68GJI\n3v72+9iesZz7/QAfehJhm2jhegzWhm4tOztOrdQExrTzMIWsvyKuONeuegUKyNSi\nNnYjNHt8Y8cX1jkIW6QhokmxroB42FVpCotzUyRUuQgepG1feREYh64f/KAFTBsr\nkhk0fozPeZtBbyCTKBxQMxgsjYODMl6pB1N3zuB+Qq6BtN4u65tY3NCJoswo7BCF\nkcC9d+8JSUQhxqo67fieJju+hune3S+e8M5ipoQnYJqNFWM1hiCDBI9ih+n7oTlB\n-----END RSA PRIVATE KEY-----\n', 'root', 'Wr12345678.', 'valid', '2019-12-13 01:38:14', '52', '系统管理员');
