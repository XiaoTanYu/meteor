<#--
/****************************************************
 * Description: 菜单的列表页面
 * Copyright:   Copyright (c) 2018
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<#include "/templates/xjj-list.ftl">
<@navList navs=navArr/>
<@content>
    <@query>
        <@button type="info" icon="glyphicon glyphicon-plus" onclick="saveJavaCode()">更新代码</@button>
        <@button type="info" icon="glyphicon glyphicon-plus" onclick="complie()">编译</@button>
        <@button type="info" icon="glyphicon glyphicon-plus" onclick="deploy()">热部署</@button>
    </@query>

</@content>

<div class="row">
    <div>
        <textarea id="java-code">

        </textarea>
    </div>
</div>
<script type="text/javascript">
    var javaData = "";
    var javaEditor;
    javaEditor = CodeMirror.fromTextArea(document.getElementById("java-code"), {
        lineNumbers: true,
        matchBrackets: true,
        mode: "text/x-java"
    });
    javaEditor.setSize('auto', '400px');

    function getJavaCode() {
        //生成源码文件
        javaData = "";
        javaEditor.setValue("");
        var workplatform_ip = '${meteor_param.arthasIp}';
        var workplatform_port = '${meteor_param.arthasPort}';
        var workplatform_agentId = '${meteor_param.arthasAgentId}';
        if (workplatform_ip == '') {
            XJJ.msger('机器IP不能为空');
            return;
        }
        if (workplatform_port == '') {
            XJJ.msger('端口号不能为空');
            return;
        }
        var className = $("#workplatform-className").val();
        var classLoaderHash = $("#workplatform-classLoaderHash").val();
        var cmd = "jad --source-only " + className;
        if (classLoaderHash != null && classLoaderHash.trim() != "") {
            cmd = "jad  -c " + classLoaderHash + " --source-only " + className;
        }
        getReturnInfo(workplatform_ip, workplatform_port, workplatform_agentId, cmd, buildJavaCode);
    }

    function buildJavaCode(data) {
        if (data != null && data == true) {
            javaEditor.setValue(javaData);
            return;
        }
        javaData = javaData + data;

    }

    //保存java代码
    function saveJavaCode() {
        var className = $("#workplatform-className").val();
        if (className == undefined || className == null || className.trim() == "") {
            XJJ.msger("类不能为空");
            return;
        }
        var names = className.split(".");
        names = "/tmp/meteor/" + names[names.length - 1] + ".java";
        var code = javaEditor.getValue();
        var script = {
            codeName: names,
            code: code
        };
        //提交表单
        $.ajax({
            url: "${base}/meteor/javacode/save", // 请求url
            type: "post", // 提交方式
            dataType: "json", // 数据类型
            data: script,// 参数序列化
            success: function (retrunData) { // 提交成功的回调函数
                if (retrunData != null && retrunData['type'] == "success") {
                    XJJ.msgok(retrunData['message']);
                } else {
                    XJJ.msger("代码更新失败，需要登录到服务器");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                XJJ.msger(textStatus);
            }
        });
    }

    //反编译

    //编译
    function complie() {
        var className = $("#workplatform-className").val();
        if (className == undefined || className == null || className.trim() == "") {
            XJJ.msger("类不能为空");
            return;
        }
        var classLoaderHash = $("#workplatform-classLoaderHash").val();
        var names = className.split(".");
        names = names[names.length - 1];
        //执行保存


        var cmd = "mc  /tmp/meteor/" + names + ".java -d /tmp/meteor";
        if (classLoaderHash != "") {
            cmd = "mc -c " + classLoaderHash + " /tmp/meteor/" + names + ".java -d /tmp/meteor";
        }
        sendCmd(cmd);
        $("#XjjTab li a[href='#meteor_arthas']").trigger("click");
    }

    //热部署
    function deploy() {
        var className = $("#workplatform-className").val();
        if (className == undefined || className == null || className.trim() == "") {
            XJJ.msger("类不能为空");
            return;
        }
        var classpath = "/tmp/meteor/" + className.replace(/\./g, "/") + ".class";
        var cmd = "redefine " + classpath;
        sendCmd(cmd);
        $("#XjjTab li a[href='#meteor_arthas']").trigger("click");
    }

</script>


