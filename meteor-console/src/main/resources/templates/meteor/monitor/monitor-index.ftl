<#--
/****************************************************
 * Description: 菜单的列表页面
 * Copyright:   Copyright (c) 2018
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<#include "/templates/xjj-list.ftl">
<@navList navs=navArr/>
<@content>
    <@query>
        <input type="hidden" id="monitor-className">
        <input type="hidden" id="monitor-method">
        <@querygroup title='监控时长(秒),5秒一次'>
            <input type="text" id="monitor-time" value="10" class="form-control input-sm" placeholder="最大150秒"
                   check-type='required number'>
        </@querygroup>
        <@button type="info" onclick="sendMonitorCmd()">重试</@button>
    </@query>

</@content>
<div class="row">
    <div class="container-fluid px-0">
        <div class="col px-0" id="monitor-console-card">
            <div id="monitor-console"></div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var monitor_ws = null;

    initMonitorConsole();

    function initMonitorConsole() {
        var arthas_ip = '${meteor_param.arthasIp}';
        var arthas_port = '${meteor_param.arthasPort}';
        var arthas_agentId = '${meteor_param.arthasAgentId}';
        if (arthas_ip == '') {
            XJJ.msger('机器IP不能为空,请在[meteor]-[全局参数设置]中进行配置');
            return;
        } else if (arthas_port == '') {
            XJJ.msger('PORT不能为空');
            return;
        }

        //获取websocket连接
        var path = 'ws://' + arthas_ip + ':' + arthas_port + '/ws';
        if (arthas_agentId != 'null' & arthas_agentId != "" && arthas_agentId != null) {
            path = path + '?method=connectArthas&id=' + arthas_agentId;
        }
        monitor_ws = initArthasConsole("monitor-console", path, "meteor_monitor",true);
    }

    function sendMonitorCmd(className, method) {
        if (className != null) {
            $("#monitor-className").val(className);
        }
        if (method != null) {
            $("#monitor-method").val(method);
        }

        var className_cmd = $("#monitor-className").val();
        var method_cmd = $("#monitor-method").val();

        var cmd = "monitor -c 5 " + className_cmd + " " + method_cmd;
        monitor_ws.send(JSON.stringify({action: 'read', data: cmd + "\r"}));

        //防止数据量过大，影响服务器
        var time = $("#monitor-time").val();
        if (time > 150) {
            time = 150;
        }
        setTimeout(function () {
            monitor_ws.send(JSON.stringify({action: 'read', data: ""}));
        }, time * 1000);
    }

</script>
