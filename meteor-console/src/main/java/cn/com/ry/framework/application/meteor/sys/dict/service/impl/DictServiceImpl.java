/****************************************************
 * Description: ServiceImpl for 数据字典
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-08-16 reywong Create File
**************************************************/

package cn.com.ry.framework.application.meteor.sys.dict.service.impl;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.framework.service.XjjServiceSupport;
import cn.com.ry.framework.application.meteor.sys.dict.dao.DictDao;
import cn.com.ry.framework.application.meteor.sys.dict.entity.DictEntity;
import cn.com.ry.framework.application.meteor.sys.dict.service.DictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DictServiceImpl extends XjjServiceSupport<DictEntity> implements DictService {

	@Autowired
	private DictDao dictDao;

	@Override
	public XjjDAO<DictEntity> getDao() {

		return dictDao;
	}
}
