/****************************************************
 * Description: Controller for 私钥信息
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
	*  2019-12-13 reywong Create File
**************************************************/
package cn.com.ry.framework.application.meteor.meteor.rsa.web;
import cn.com.ry.framework.application.meteor.meteor.rsa.entity.RsaEntity;
import cn.com.ry.framework.application.meteor.meteor.rsa.service.RsaService;
import java.util.Date;
import cn.com.ry.framework.application.meteor.framework.json.XjjJson;
import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.framework.utils.Excel2007Util;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import cn.com.ry.framework.application.meteor.framework.web.support.Pagination;
import cn.com.ry.framework.application.meteor.framework.web.support.QueryParameter;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.ry.framework.application.meteor.framework.security.annotations.SecCreate;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecDelete;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecEdit;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecList;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecPrivilege;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/meteor/rsa")
public class RsaController extends SpringControllerSupport{
	@Autowired
	private RsaService rsaService;


	@SecPrivilege(title="私钥信息")
	@RequestMapping(value = "/index")
	public String index(Model model) {
		String page = this.getViewPath("index");
		return page;
	}

	@SecList
	@RequestMapping(value = "/list")
	public String list(Model model,
			@QueryParameter XJJParameter query,
			@ModelAttribute("page") Pagination page
			) {
		page = rsaService.findPage(query,page);
		return getViewPath("list");
	}

	@SecCreate
	@RequestMapping("/input")
	public String create(@ModelAttribute("rsa") RsaEntity rsa,Model model){
		return getViewPath("input");
	}

	@SecEdit
	@RequestMapping("/input/{id}")
	public String edit(@PathVariable("id") Long id, Model model){
		RsaEntity rsa = rsaService.getById(id);
		model.addAttribute("rsa",rsa);
		return getViewPath("input");
	}

	@SecCreate
	@SecEdit
	@RequestMapping("/save")
	@ResponseBody
	public XjjJson save(@ModelAttribute RsaEntity rsa){

		validateSave(rsa);
		if(rsa.isNew())
		{
			//rsa.setCreateDate(new Date());
			rsaService.save(rsa);
		}else
		{
			rsaService.update(rsa);
		}
		return XjjJson.success("保存成功");
	}


	/**
	 * 数据校验
	 **/
	private void validateSave(RsaEntity rsa){
		//必填项校验
		// 判断私钥名称是否为空
		if(null==rsa.getRsaName()){
			throw new ValidationException("校验失败，私钥名称不能为空！");
		}
		// 判断私钥内容是否为空
		if(null==rsa.getRsaValue()){
			throw new ValidationException("校验失败，私钥内容不能为空！");
		}
		// 判断私钥用户是否为空
		if(null==rsa.getRsaUsername()){
			throw new ValidationException("校验失败，私钥用户不能为空！");
		}
		// 判断状态是否为空
		if(null==rsa.getStatus()){
			throw new ValidationException("校验失败，状态不能为空！");
		}
		// 判断创建时间是否为空
		if(null==rsa.getCreateTime()){
			throw new ValidationException("校验失败，创建时间不能为空！");
		}
		// 判断创建人员ID是否为空
		if(null==rsa.getCreatePersonId()){
			throw new ValidationException("校验失败，创建人员ID不能为空！");
		}
		// 判断创建人员姓名是否为空
		if(null==rsa.getCreatePersonName()){
			throw new ValidationException("校验失败，创建人员姓名不能为空！");
		}
	}

	@SecDelete
	@RequestMapping("/delete/{id}")
    @ResponseBody
	public XjjJson delete(@PathVariable("id") Long id){
		rsaService.delete(id);
		return XjjJson.success("成功删除1条");
	}
	@SecDelete
	@RequestMapping("/delete")
    @ResponseBody
	public XjjJson delete(@RequestParam("ids") Long[] ids){
		if(ids == null || ids.length == 0){
			return XjjJson.error("没有选择删除记录");
		}
		for(Long id : ids){
			rsaService.delete(id);
		}
		return XjjJson.success("成功删除"+ids.length+"条");
	}

    /**
    * 导入用户
    * @param model
    * @return
    */
    @RequestMapping(value = "/import")
    public String importExcel(Model model) {
        return getViewPath("import");
    }
    /**
    * 导入
    * @param model
    * @return
    */
	@SecCreate
	@SecEdit
    @RequestMapping(value = "/import/save")
    @ResponseBody
    public  XjjJson saveImport(Model model,@RequestParam (value="fileId",required=false) Long fileId) {
        System.out.println("上传开始----");
        try {
            Map<String,Object> map = rsaService.saveImport(fileId);
            int allCnt = (Integer)map.get("allCnt");
            return XjjJson.success("导入成功：本次共计导入数据"+allCnt+"条");
        } catch (ValidationException e) {
            return XjjJson.error("导入失败：<br/>"+e.getMessage());
        }
    }

    /**
    * 导出用户信息
    * @param request
    * @param response
    * @return
    */
	@SecList
    @RequestMapping(value = "/export/excel")
    public String exportExcel(HttpServletRequest request,HttpServletResponse response) {
        List<RsaEntity>  userList =rsaService.findAll();
        LinkedHashMap<String, String> columns = new LinkedHashMap<String, String>();
        columns.put("id", "主键ID");
        columns.put("rsaName", "私钥名称");
        columns.put("rsaValue", "私钥内容");
        columns.put("rsaUsername", "私钥用户");
        columns.put("rsaPassword", "私钥密码");
        columns.put("status", "状态");
        columns.put("createTime", "创建时间");
        columns.put("createPersonId", "创建人员ID");
        columns.put("createPersonName", "创建人员姓名");
        Excel2007Util.write(userList, columns,response,"rsa-export");
        return null;
    }
}

