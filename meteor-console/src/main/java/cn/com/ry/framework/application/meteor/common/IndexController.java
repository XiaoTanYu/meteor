package cn.com.ry.framework.application.meteor.common;

import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class IndexController extends SpringControllerSupport {

	@RequestMapping(value = "/")
	public String index() {
		return "redirect:/passport/manager/login";
	}


}
