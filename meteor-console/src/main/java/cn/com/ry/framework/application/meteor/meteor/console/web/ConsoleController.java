package cn.com.ry.framework.application.meteor.meteor.console.web;

import cn.com.ry.framework.application.meteor.framework.security.annotations.SecList;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecPrivilege;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/meteor/console")
public class ConsoleController extends SpringControllerSupport {

    @SecPrivilege(title = "Arthas控制台")
    @SecList
    @RequestMapping(value = "/index")
    public String index(Model model) {
        String page = this.getViewPath("index");
        return page;
    }

    @RequestMapping(value = "/help")
    public String help(Model model) {
        String page = this.getViewPath("help");
        return page;
    }

    @RequestMapping(value = "/terminal/{cid}")
    public String terminal(Model model, @PathVariable String cid) {
        String page = this.getViewPath("terminal");
        model.addAttribute("cid", cid);
        return page;
    }
}
